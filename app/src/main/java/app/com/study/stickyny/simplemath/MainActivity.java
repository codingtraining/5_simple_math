package app.com.study.stickyny.simplemath;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView mResultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText firstNumEdit = (EditText) findViewById(R.id.first_num_edit);
        final EditText secondNumEdit = (EditText) findViewById(R.id.second_num_edit);
        mResultText = (TextView) findViewById(R.id.result_text);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 0) {
                    if (i != charSequence.length()) {
                        if (!isNum(charSequence.charAt(i))) {
                            Toast.makeText(MainActivity.this, getString(R.string.error_msg_not_num), Toast.LENGTH_SHORT).show();

                            if (firstNumEdit.getText().toString().equals(charSequence.toString())) {
                                firstNumEdit.setText("");
                            } else {
                                secondNumEdit.setText("");
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!firstNumEdit.getText().toString().isEmpty() && !secondNumEdit.getText().toString().isEmpty()) {
                    print(firstNumEdit.getText().toString(), secondNumEdit.getText().toString());
                }
            }
        };

        firstNumEdit.addTextChangedListener(textWatcher);
        secondNumEdit.addTextChangedListener(textWatcher);
    }

    private boolean isNum(Character temp) {
        String[] num = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        int sum = 0;

        for (String s : num) {
            if (temp.toString().equals(s)) {
                sum ++;
            }
        }

        if (sum != 0 || temp==null) {
            return true;
        } else {
            return false;
        }
    }

    private void print(String num1, String num2) {
        mResultText.setText(calculator(stringToNum(num1), (stringToNum(num2))));
    }

    private int stringToNum(String str) {
        return Integer.parseInt(str.trim());
    }

    private String calculator(int num1, int num2) {
        return num1+" + "+num2+" = "+(num1+num2)+"\n"+num1+" - "+num2+" = "+(num1-num2)+"\n"+num1+" * "+num2+" = "+(num1*num2)+"\n"+num1+" / "+num2+" = "+(num1/num2);
    }
}
